/**
 * 
 */
package lab4;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author frankx
 *
 */
public class QuickFindUFTest {
	
	private QuickFindUF	 uf;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		uf = new QuickFindUF(10);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link lab4.QuickFindUF#QuickFindUF(int)}.
	 */
	@Test
	public void testQuickFindUF() {
		for (int i = 0; i < 10; i++) assertSame(i, uf.find(i));
	}

	/**
	 * Test method for {@link lab4.QuickFindUF#count()}.
	 */
	@Test
	public void testCount() {
		assertSame(uf.count(),10);
	}

	/**
	 * Test method for {@link lab4.QuickFindUF#find(int)}.
	 */
	@Test
	public void testFind() {
        assertSame(0, uf.find(0));
        
        uf.union(0,9);
        assertSame(9, uf.find(0));
	}

	/**
	 * Test method for {@link lab4.QuickFindUF#connected(int, int)}.
	 */
	@Test
	public void testConnected() {
		//test sites not connected
		for (int i = 1; i < 10; i++) {
            assertFalse(uf.connected(0, i));
        }
		
		//test connections
		uf.union(0, 1);
        uf.union(9, 8);
        assertTrue(uf.connected(1, 0));
        assertTrue(uf.connected(9, 8));
       
        uf.union(8, 1);
        assertTrue(uf.connected(0, 9));
		
		
	}

	/**
	 * Test method for {@link lab4.QuickFindUF#union(int, int)}.
	 */
	@Test
	public void testUnion() {
		uf.union(0, 1);
        uf.union(1, 2);
        uf.union(2, 3);
        uf.union(3, 4);
        assertSame(4, uf.find(0));
        assertNotSame(uf.find(0),uf.find(5));
	}

}
